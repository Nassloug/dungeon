import java.util.Scanner;

public class Battle {

	private int monsterLife;
	private int monsterDamages;
	private String monsterName;
	private Player player;
	private boolean stopBattle;
	private final Scanner scanner = new Scanner(System.in);
	
	public Battle(int monsterLife, int monsterDamages, String monsterName, Player player){
		this.monsterLife = monsterLife;
		this.monsterDamages = monsterDamages;
		this.monsterName = monsterName;
		this.player = player;
		this.stopBattle = false;
	}
	
	private void playerAttack(){
		System.out.println("You strike with "+player.getWeapon()+"!\n"+monsterName+" takes "+player.getDamages()+" damage points!");
		monsterLife -= player.getDamages();
		if(monsterLife<=0){
			System.out.println(monsterName+" is dead!\nYou are a tough one!");
			stopBattle = true;
		}
	}
	
	private boolean ennemyAttack(){
		System.out.println(monsterName+" attacks you!\nYou take "+monsterDamages+" damage points!");
		player.setLife(player.getLife()-monsterDamages);
		if(player.getLife()<=0){
			System.out.println("You are dead!");
			return true;
		}
		return false;
	}
	
	//Interprets commands entered by the player during battle
	private boolean interpretCommand(String command){
		switch(command){
		case "What can I do ?":
			System.out.println("Here are the actions you can perform:\n" +
					"State\n" +
					"Attack\n" +
					"Escape\n" +
					"Use Potion\n" +
					"Surrender");
			System.out.println("What do you want to do?");
			String line = scanner.nextLine();
			return interpretCommand(line);
		case "State":
			player.getState();
			System.out.println("What do you want to do?");
			String line2 = scanner.nextLine();
			return interpretCommand(line2);
		case "Attack":
			playerAttack();
			break;
		case "Use Potion":
			if(player.usePotion()){
				break;
			}
			else{
				System.out.println("What do you want to do?");
				String newLine = scanner.nextLine();
				return interpretCommand(newLine);
			}
		case "Surrender":
			System.out.println("You kneel in front of "+monsterName);
			return true;
		case "Escape":
			if(Math.round(Math.random())==1){ //Player can fail to escape
				System.out.println("You managed to escape!");
				stopBattle = true;
			}
			else{
				System.out.println("You failed to escape!");
			}
			break;
		default :
			System.out.println("This is not valid...Can you repeat please?");
			System.out.println("What do you want to do?");
			String newLine = scanner.nextLine();
			return interpretCommand(newLine);
		}
		return false;
	}
	
	public String getMonster(){
		return monsterName;
	}
	
	public Player getPlayer(){
		return player;
	}
	
	public boolean Start(){
		System.out.println("You are now engaged in a battle against "+monsterName);
		do{
			System.out.println("What do you want to do?");
			String line = scanner.nextLine();
			if(interpretCommand(line)){
				return false;
			}
			if(!stopBattle){
				if(ennemyAttack()){
					return false;
				}
			}
		}while(!stopBattle);
		return true;
	}
}
