
public class BossBattle extends Battle{
	
	public BossBattle(int monsterLife, int monsterDamages, String monsterName, Player player){
		super(monsterLife,monsterDamages,monsterName,player);	
	}
	
	public boolean Start(){
		if(super.Start()){
			super.getPlayer().end();
			System.out.println("You have slain this dispeakable creature!\nYou are now a glorious adventurer!");
			return true;
		}
		else{
			return false;
		}
	}
	
}
