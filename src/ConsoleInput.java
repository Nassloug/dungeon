import java.util.concurrent.*;

public class ConsoleInput {
	private final int time;
	private final TimeUnit unit;

	public ConsoleInput(int time, TimeUnit unit) {
		this.time = time;
		this.unit = unit;
	}

	public String readLine() throws InterruptedException {
		ExecutorService executor = Executors.newSingleThreadExecutor(); //Use ThreadFactory to create new thread when needed
		String line = "";
		try{
			Future<String> result = executor.submit(new ConsoleInputReadTask());
			try{
				line = result.get(time,unit); //Will throw TimeoutException after "time"
			}catch(ExecutionException e){ //If anything goes wrong
				System.out.println(e.getMessage());
			}catch(TimeoutException e){ //Triggers if we reach time without reading any line
				System.out.println("Too late...");
				result.cancel(true);
			}
		}finally{
			executor.shutdownNow(); //Stops all tasks of executor
		}
		return line;
	}
}