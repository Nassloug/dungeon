import java.io.*;
import java.util.concurrent.Callable;

public class ConsoleInputReadTask implements Callable<String> {
	
	public String call() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input;
		do{
			try{
				while(!br.ready()){ //Waits until BufferReader contains data to read
					Thread.sleep(200);
				}
				input = br.readLine();
			}catch (InterruptedException e) {
				return null;
			}
		}while(input.equals("")); //Leaves the loop when a line is entered by user
		return input;
	}
	
}