import java.util.HashMap;
import java.util.Scanner;

public class Dungeon {

	private String name;
	private HashMap<Integer,Room> dungeonMap;
	private int roomNb;
	private final Scanner scanner = new Scanner(System.in);
	private Player player;
	private boolean stopGame;
	private int roomId;
	private Room currentRoom;
	
	public Dungeon(String name){
		this.name = name;
		this.roomNb = 0;
		this.stopGame = false;
		this.dungeonMap = new HashMap<Integer,Room>();
	}
	
	//Used each time a yes/no question is asked to the player
	private boolean yesnoAnswer(){
		while(true){
			String answer = scanner.nextLine();
			switch(answer){
			case "Yes":
				return true;
			case "No":
				return false;
			}
			System.out.println("This is not valid...Can you repeat please?");
		}
	}
	
	public void addRoom(Room room){
		this.dungeonMap.put(roomNb,room);
		roomNb++;
	}
	
	public Room getRoom(int key){
		return this.dungeonMap.get(key);
	}
	
	//Interprets commands entered by the player
	private void interpretCommand(String command){
		switch(command){
		case "What can I do ?":
			System.out.println("Here are the actions you can perform:\n" +
					"State\n" +
					"Look around\n" +
					"Go\n" +
					"  West\n"+
					"  North\n"+
					"  East\n"+
					"  South\n"+
					"Unlock\n" +
					"  West\n"+
					"  North\n"+
					"  East\n"+
					"  South\n"+
					"Look closer\n" +
					"  West\n"+
					"  North\n"+
					"  East\n"+
					"  South\n"+
					"Surrender");
			if(currentRoom.isTreasure()){
				System.out.println("Take treasure");
			}
			break;
		case "State":
			player.getState();
			break;
		case "Look around":
			System.out.println(currentRoom.describe());
			break;
		case "Look closer South":
			if(currentRoom.getWayState(0)==2){
				currentRoom.alterWay(0);
				System.out.println("There is a hidden path!\nDo you want to go that way?");
				if(yesnoAnswer()){
					System.out.println("You go through the southern way...");
					this.player.setRoom(currentRoom.getNextRoom(0,roomId));
				}
			}
			else{
				System.out.println(currentRoom.getWayStateDescription(0));
			}
			break;
		case "Look closer West":
			if(currentRoom.getWayState(1)==2){
				currentRoom.alterWay(1);
				System.out.println("There is a hidden path!\nDo you want to go that way?");
				if(yesnoAnswer()){
					System.out.println("You go through the western way...");
					this.player.setRoom(currentRoom.getNextRoom(1,roomId));
				}
			}
			else{
				System.out.println(currentRoom.getWayStateDescription(1));
			}
			break;
		case "Look closer North":
			if(currentRoom.getWayState(2)==2){
				currentRoom.alterWay(2);
				System.out.println("There is a hidden path!\nDo you want to go that way?");
				if(yesnoAnswer()){
					System.out.println("You go through the northern way...");
					this.player.setRoom(currentRoom.getNextRoom(2,roomId));
				}
			}
			else{
				System.out.println(currentRoom.getWayStateDescription(2));
			}
			break;
		case "Look closer East":
			if(currentRoom.getWayState(3)==2){
				currentRoom.alterWay(3);
				System.out.println("There is a hidden path!\nDo you want to go that way?");
				if(yesnoAnswer()){
					System.out.println("You go through the eastern way...");
					this.player.setRoom(currentRoom.getNextRoom(3,roomId));
				}
			}
			else{
				System.out.println(currentRoom.getWayStateDescription(3));
			}
			break;
		case "Take treasure":
			if(currentRoom.isTreasure()){
				System.out.println("You open the treasure chest...");
				currentRoom.getTreasure(player);
			}
			else{
				System.out.println("There is no treasure to take in this room");
			}
			break;
		case "Unlock South":
			if(currentRoom.getWayState(0)==1){
				System.out.println("Do you want to use a key on this door?");
				if(yesnoAnswer()){
					if(player.useKey()){
						System.out.println("The door is now unlocked");
						currentRoom.alterWay(0);
					}
					else{
						System.out.println("The door is still locked");
					}
				}
			}
			else{
				System.out.println("There is nothing to unlock...");
			}
			break;
		case "Unlock West":
			if(currentRoom.getWayState(1)==1){
				System.out.println("Do you want to use a key on this door?");
				if(yesnoAnswer()){
					if(player.useKey()){
						System.out.println("The door is now unlocked");
						currentRoom.alterWay(1);
					}
					else{
						System.out.println("The door is still locked");
					}
				}
			}
			else{
				System.out.println("There is nothing to unlock...");
			}
			break;
		case "Unlock North":
			if(currentRoom.getWayState(2)==1){
				System.out.println("Do you want to use a key on this door?");
				if(yesnoAnswer()){
					if(player.useKey()){
						System.out.println("The door is now unlocked");
						currentRoom.alterWay(2);
					}
					else{
						System.out.println("The door is still locked");
					}
				}
			}
			else{
				System.out.println("There is nothing to unlock...");
			}
			break;
		case "Unlock East":
			if(currentRoom.getWayState(3)==1){
				System.out.println("Do you want to use a key on this door?");
				if(yesnoAnswer()){
					if(player.useKey()){
						System.out.println("The door is now unlocked");
						currentRoom.alterWay(3);
					}
					else{
						System.out.println("The door is still locked");
					}
				}
			}
			else{
				System.out.println("There is nothing to unlock...");
			}
			break;
		case "Surrender":
			System.out.println("You failed your quest...too bad...");
			this.stopGame = true;
			break;
		case "Go South":
			if(player.getLastRoomWay()==0){
				System.out.println("You go back on your feet...");
			}
			if(currentRoom.getWayState(0)==0){
				if(roomId==0){
					System.out.println("You left the dungeon...");
					System.out.println("Do you want to go back inside?");
					if(yesnoAnswer()){
						System.out.println("You are now entering the dungeon...again...");
					}
					else{
						System.out.println("You coward! Come back in there!");
						if(yesnoAnswer()){
							System.out.println("Good...\nYou are now entering the dungeon...again...");
						}
						else{
							System.out.println("I am way too old to argue with such a weak-willed person!\nYou failed!");
							this.stopGame = true;
						}
					}
				}
				else{
					System.out.println("You go through the southern way...");
					this.player.setRoom(currentRoom.getNextRoom(0,roomId));
					this.player.setLastRoomWay(2);
				}
			}
			else{
				System.out.println(currentRoom.getWayStateDescription(0));
			}
			break;
		case "Go West":
			if(player.getLastRoomWay()==1){
				System.out.println("You go back on your feet...");
			}
			if(currentRoom.getWayState(1)==0){
				System.out.println("You go through the western way...");
				this.player.setRoom(currentRoom.getNextRoom(1,roomId));
				this.player.setLastRoomWay(3);
			}
			else{
				System.out.println(currentRoom.getWayStateDescription(1));
			}
			break;
		case "Go North":
			if(player.getLastRoomWay()==2){
				System.out.println("You go back on your feet...");
			}
			if(currentRoom.getWayState(2)==0){
				System.out.println("You go through the northern way...");
				this.player.setRoom(currentRoom.getNextRoom(2,roomId));
				this.player.setLastRoomWay(0);
			}
			else{
				System.out.println(currentRoom.getWayStateDescription(2));
			}
			break;
		case "Go East":
			if(player.getLastRoomWay()==3){
				System.out.println("You go back on your feet...");
			}
			if(currentRoom.getWayState(3)==0){
				System.out.println("You go through the eastern way...");
				this.player.setRoom(currentRoom.getNextRoom(3,roomId));
				this.player.setLastRoomWay(1);
			}
			else{
				System.out.println(currentRoom.getWayStateDescription(3));
			}
			break;
		default :
			System.out.println("This is not valid...Can you repeat please?");
		}
	}
	
	public Player Start(Player player){
		this.player = player;
		System.out.println("You are now entering "+name+"...");
		do{
			roomId = player.getRoom();
			currentRoom = dungeonMap.get(roomId);
			if(!currentRoom.Activate(player)||player.isEnd()){
				stopGame = true;
			}
			else{
				if(currentRoom.isMonster()){
					System.out.println("There is a monster in this room!\nThis is "+currentRoom.getBattle().getMonster());
					if(!currentRoom.startBattle()){
						System.out.println("You failed your quest...too bad...");
						return player;
					}
				}
				if(player.isEnd()){
					stopGame = true;
				}
				else{
					System.out.println("What do you want to do?");
					String line = scanner.nextLine();
					interpretCommand(line);
				}
			}
		}while(!stopGame);
		player.setRoom(0);
		return player;
	}
}
