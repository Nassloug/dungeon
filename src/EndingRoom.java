public class EndingRoom extends Room{
	
	public EndingRoom(Exit[] ways){
		super(false,false,ways,"");
	}
	
	public boolean Activate(Player player){
		System.out.println("You ended this level!");
		player.end();
		return true;
	}
	
}
