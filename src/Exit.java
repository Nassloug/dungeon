
public abstract class Exit{

	private int[] linkedRooms;
	
	public Exit(int roomOne, int roomTwo){
		this.linkedRooms = new int[2];
		linkedRooms[0] = roomOne;
		linkedRooms[1] = roomTwo;
	}
	
	public int[] getLinkedRooms(){
		return this.linkedRooms;
	}
	
	public void setLinkedRooms(int roomOne, int roomTwo){
		this.linkedRooms = new int[]{roomOne,roomTwo};
	}
	
	public int getNextRoom(int currentRoom){
		if(linkedRooms[0]==currentRoom){
			return linkedRooms[1];
		}
		else if(linkedRooms[1]==currentRoom){
			return linkedRooms[0];
		}
		else{
			System.out.println("Which does not exist so you stay there...");
			return currentRoom;
		}
	}
	
	public abstract int getType();
	
	public abstract void alterWay(); 
}
