
public class HiddenWay extends Exit{

	private boolean hidden;
	private int type = 2;
	
	public HiddenWay(int roomOne,int roomTwo){
		super(roomOne, roomTwo);
		this.hidden = true;
	}
	
	public void discover(){
		this.hidden = false;
	}
	
	public int getNextRoom(int currentRoom){
		if(!hidden){
			return super.getNextRoom(currentRoom);
		}
		else{
			return currentRoom;
		}
	}
	
	public boolean isHidden(){
		return this.hidden;
	}
	
	public int getType(){
		return this.type;
	}
	
	public void alterWay(){
		this.discover();
		this.type = 0;
	}
}
