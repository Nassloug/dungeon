
public class LockedWay extends Exit{

	private boolean locked;
	private int type = 1;
	
	public LockedWay(int roomOne,int roomTwo){
		super(roomOne, roomTwo);
		this.locked = true;
	}
	
	public void unlock(){
		this.locked = false;
	}
	
	public int getNextRoom(int currentRoom){
		if(!locked){
			return super.getNextRoom(currentRoom);
		}
		else{
			return currentRoom;
		}
	}
	
	public boolean isLocked(){
		return this.locked;
	}
	
	public int getType(){
		return this.type;
	}
	
	public void alterWay(){
		this.unlock();
		this.type = 0;
	}
	
}
