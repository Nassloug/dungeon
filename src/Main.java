
public class Main {
	
	public static void main(String[] args){
		
		Player player = new Player();
		Dungeon dungeon = new Dungeon("The Dungeon");
		Dungeon lastBoss = new Dungeon("The Elder Dragon Cave");
		Exit[] ways = new Exit[4];
		ways[0] = new Way(0,0);
		ways[1] = new Way(0,1);
		ways[2] = new Way(0,2);
		ways[3] = new Way(0,3);
		dungeon.addRoom(new Room(false,false,ways,"This is the entrance"));
		Exit[] ways2 = new Exit[4];
		ways2[0] = new Way(1,1);
		ways2[1] = new Way(1,1);
		ways2[2] = new Way(1,1);
		ways2[3] = new Way(1,0);
		dungeon.addRoom(new Room(true,false,ways2,"There are blood stains in this room"));
		dungeon.getRoom(1).setBattle(new Battle(5,1,"Jean-Jacques",player));
		Exit[] ways3 = new Exit[4];
		ways3[0] = new Way(2,0);
		ways3[1] = new LockedWay(2,4);
		ways3[2] = new Way(2,2);
		ways3[3] = new Way(2,2);
		dungeon.addRoom(new Trap(false,false,ways3,"There is a rope linked to an arrow launcher"));
		Exit[] ways4 = new Exit[4];
		ways4[0] = new Way(3,3);
		ways4[1] = new Way(3,0);
		ways4[2] = new HiddenWay(3,5);
		ways4[3] = new Way(3,3);
		dungeon.addRoom(new Room(false,false,ways4,"There is something mysterious about this room"));
		Exit[] ways5 = new Exit[4];
		for(int i=0;i<4;i++){
			ways5[i] = new Way(4,4);
		}
		dungeon.addRoom(new EndingRoom(ways5));
		Exit[] ways6 = new Exit[4];
		ways6[0] = new Way(5,3);
		ways6[1] = new Way(5,5);
		ways6[2] = new Way(5,5);
		ways6[3] = new Way(5,5);
		dungeon.addRoom(new Room(false,true,ways6,"There is room is wisely decorated...I am responsible for this\n"
				+ "I was dungeon decorator before I became dungeon master"));
		Treasure chest = new Treasure(true,2,true);
		chest.setWeapon(4,"Iron Sword");
		dungeon.getRoom(5).setTreasure(chest);
		if(dungeon.Start(player).isEnd()){
			player.notEnd();
			Exit[] ways7 = new Exit[4];
			ways7[0] = new Way(0,0);
			ways7[1] = new Way(0,0);
			ways7[2] = new Way(0,1);
			ways7[3] = new Way(0,0);
			lastBoss.addRoom(new Room(false,true,ways7,"The next room will be your judgement"));
			Treasure bossChest = new Treasure(false,0,true);
			bossChest.setWeapon(10,"Legendary Sword");
			lastBoss.getRoom(0).setTreasure(bossChest);
			Exit[] ways8 = new Exit[4];
			ways8[0] = new Way(1,0);
			ways8[1] = new Way(1,1);
			ways8[2] = new Way(1,1);
			ways8[3] = new Way(1,1);
			lastBoss.addRoom(new Room(true,true,ways8,"This is the final room"));
			lastBoss.getRoom(1).setBattle(new BossBattle(100,2,"Elder Dragon",player));
			lastBoss.Start(player);
		}
		System.out.println("\nThanks for playing! XOXO");
	}
	
}
