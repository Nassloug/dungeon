
public class Player {

	private int life;
	private int keys;
	private int potions;
	private int damages;
	private String weapon;
	private int room;
	private int lastRoomWay;
	private boolean end;
	
	public Player(){
		this.life = 10;
		this.keys = 0;
		this.potions = 0;
		this.damages = 2;
		this.weapon = "Wooden Sword";
		this.room = 0;
		this.lastRoomWay = 0;
		this.end = false;
	}
	
	public int getLife(){
		return this.life;
	}
	
	public void setLife(int life){
		this.life = life;
	}
	
	public int getRoom(){
		return this.room;
	}
	
	public void setRoom(int room){
		this.room = room;
	}
	
	public int getLastRoomWay(){
		return this.lastRoomWay;
	}
	
	public void setLastRoomWay(int way){
		this.lastRoomWay = way;
	}
	
	public int getKeys(){
		return this.keys;		
	}
	
	public void addKey(){
		System.out.println("You found a key");
		this.keys++;
	}
	
	public boolean useKey(){
		if(keys>0){
			System.out.println("You use a key to unlock the door");
			this.keys--;
			return true;
		}
		else{
			System.out.println("You have no key...");
			return false;
		}
	}
	
	public int getPotions(){
		return this.potions;
	}
	
	public void addPotions(int amount){
		System.out.println("You found "+amount+" potions");
		this.potions += amount;
	}
	
	public boolean usePotion(){
		if(potions>0){
			System.out.println("You drink a life potion to restore your health\nYou feel better than ever!");
			this.potions --;
			this.life = 10;
			return true;
		}
		else{
			System.out.println("You have no potion...");
			return false;
		}
	}
	
	public int getDamages(){
		return this.damages;
	}
	
	public String getWeapon(){
		return this.weapon;
	}
	
	public void setWeapon(int newDamages, String name){
		this.damages = newDamages;
		this.weapon = name;
		System.out.println("You are now carrying "+name+"\nThis new weapon deals "+newDamages+" damage points to your foes!");
	}
	
	//Returns the player states
	public void getState(){
		System.out.println("Life points: "+life);
		System.out.println("Weapon: "+weapon);
		System.out.println("	Damages: "+damages);
		System.out.println("Keys: "+keys);
		System.out.println("Potions: "+potions);
	}
	
	public void end(){
		this.end = true;
	}
	
	public void notEnd(){
		this.end = false;
	}
	
	public boolean isEnd(){
		return end;
	}

}
