
public class Room {

	private boolean monster;
	private boolean treasure;
	private Exit[] ways;
	private String description;
	private String waysState;
	private Battle battle;
	private Treasure chest;

	public Room(boolean monster, boolean treasure, Exit[] ways, String description){
		this.monster = monster;
		this.treasure = treasure;
		this.ways = ways;
		this.description = description;
		setWaysState();
		
	}
	
	public void setWaysState(){
		this.waysState = "\nOn the southern side of the room..." + this.getWayStateDescription(0) + 
				"\nOn the western side of the room..." + this.getWayStateDescription(1) +
				"\nOn the northern side of the room..." + this.getWayStateDescription(2) +
				"\nOn the eastern side of the room..." + this.getWayStateDescription(3);
	}
	
	public void setMonster(boolean state){
		this.monster = state;
	}
	
	public boolean isMonster(){
		return this.monster;
	}
	
	public void setTreasure(boolean state){
		this.treasure = state;
	}
	
	public boolean isTreasure(){
		return this.treasure;
	}
	
	public String describe(){
		String result = this.description;
		result += waysState;
		if(treasure){
			if(treasure){
				result += "\nThere is a treasure right in the middle of the room!";
			}
		}
		return result;
	}
	
	public void setDescription(String newDescription){
		this.description = newDescription;
	}
	
	//Changes the state of a way
	public void alterWay(int id){
		this.ways[id].alterWay();
		setWaysState();
	}
	
	//Returns the state of a way, used for text display
	public String getWayStateDescription(int id){
		if(this.ways[id].getType()==0){
			return "You can quit the room by this way";
		}
		if(this.ways[id].getType()==1){
			return "There is a locked door";
		}
		if(this.ways[id].getType()==2){
			return "You see nothing there...but maybe you should look closer";
		}
		else{
			return "There is nothing there";
		}
	}
	
	public Battle getBattle(){
		return this.battle;
	}
	
	public void setBattle(Battle battle){
		this.battle = battle;
	}
	
	public boolean startBattle(){
		if(this.battle.Start()){
			this.monster = false;
			return true;
		}
		else{
			return false;
		}
	}
	
	public void setTreasure(Treasure chest){
		this.chest = chest;
	}
	
	//Gives content of "chest" to "player"
	public void getTreasure(Player player){
		chest.getTreasure(player);
		this.treasure = false;
	}
	
	//Returns 0 if unlocked, 1 if locked, 2 if hidden
	public int getWayState(int id){
		return this.ways[id].getType();
	}
	
	//Returns the id of the next room following "direction"
	public int getNextRoom(int direction, int currentRoom){
		return this.ways[direction].getNextRoom(currentRoom);
	}
	
	public boolean Activate(Player player){
		return true;
	}
}
