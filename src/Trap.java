import java.util.concurrent.TimeUnit;

public class Trap extends Room{
	
	private boolean active;
	private ConsoleInput reactivity;
	
	public Trap(boolean monster, boolean treasure, Exit[] ways, String description){
		super(monster, treasure, ways, description);
		this.active = true;
	}
	
	//Will be activate when player enters the room
	public boolean Activate(Player player){
		if(active){
			reactivity = new ConsoleInput(5,TimeUnit.SECONDS); //Input handling concurrency! Unbelievable!
			String line = "";
		    System.out.println("This is a trap! Arrows are coming in your direction!\nDo something!");
		    try {
		    	line = reactivity.readLine(); //Triggers ConsoleInput 
		    }catch (InterruptedException e) {}
		    if(line.equals("Dodge")){
		    	System.out.println("You succesfully dodged the arrows");
		    }else{
				System.out.println("You failed to dodge...\nYou was shot by arrows\nYou lose 2 life points");
				player.setLife(player.getLife()-2);
				if(player.getLife()<=0){
					System.out.println("What a sad way to die...");
					return false;
				}
			}
		}
		this.active = false;
		return true;
	}
}
