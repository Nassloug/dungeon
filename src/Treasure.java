
public class Treasure {

	private boolean isKey;
	private int potions;
	private boolean isWeapon;
	private String weaponName;
	private int weaponDamages;
	
	public Treasure(boolean isKey, int potions, boolean isWeapon){
		this.isKey = isKey;
		this.potions = potions;
		this.isWeapon = isWeapon;
	}
	
	public void setWeapon(int damages, String name){
		if(isWeapon){
			this.weaponDamages = damages;
			this.weaponName = name;
		}
	}
	
	public  void getTreasure(Player player){
		if(isKey){
			player.addKey();
		}
		if(potions>0){
			player.addPotions(potions);
		}
		if(isWeapon){
			player.setWeapon(weaponDamages, weaponName);
		}
	}
}
