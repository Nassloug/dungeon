
public class Way extends Exit{

	private int type = 0;
	
	public Way(int roomOne,int roomTwo){
		super(roomOne, roomTwo);
		if(roomOne==roomTwo){
			type = -1;
		}
	}
	
	public int getNextRoom(int currentRoom){
		return super.getNextRoom(currentRoom);

	}
	
	public int getType(){
		return this.type;
	}
	
	public void alterWay(){
		this.type = 0;
	}
	
}
