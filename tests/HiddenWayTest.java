import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HiddenWayTest {

	HiddenWay hiddenWay;
	
	@Before
	public void setUp() throws Exception {
		hiddenWay = new HiddenWay(0, 1);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDiscover() {
		hiddenWay.discover();
		assertFalse(hiddenWay.isHidden());
	}
	
	@Test
	public void testGetType(){
		assertEquals(2, hiddenWay.getType());
	}
	
	@Test
	public void testGetNextRoom(){
		assertEquals(0, hiddenWay.getNextRoom(0));
	}

}
