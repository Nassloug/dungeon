import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LockedWayTest {

	LockedWay lockedWay;
	
	@Before
	public void setUp() throws Exception {
		lockedWay = new LockedWay(0, 1);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testUnlock() {
		lockedWay.unlock();
		assertFalse(lockedWay.isLocked());
	}
	
	@Test
	public void testGetType(){
		assertEquals(1, lockedWay.getType());
	}
	
	@Test
	public void testGetNextRoom(){
		assertEquals(0, lockedWay.getNextRoom(0));
	}

}
