import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

public class RoomTest {

	private Exit[] ways;
	private Room room;
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		ways = new Exit[4];
		for (int i=0; i<4; i++){
			ways[i] = new LockedWay(0,0);
		}
		room = new Room(false, false, ways, "this is the entrance");
	}

	@After
	public void tearDown() throws Exception {
	}	
	
	@Test
	public void testSetMonster(){
		room.setMonster(true);
		assertTrue(room.isMonster());
	}
	
	@Test
	public void testIsMonster(){
		room.setMonster(true);
		assertTrue(room.isMonster());
	}
	
	@Test
	public void testIsTreasure(){
		assertFalse(room.isTreasure());
	}
	
	@Test
	public void testSetTreasure(){
		room.setTreasure(true);
		assertTrue(room.isTreasure());
	}
	
	@Test
	public void testDescribe(){
		String result = "this is the entrance"+
				"\nOn the southern side of the room..."+"There is a locked door"+ 
				"\nOn the western side of the room..." + "There is a locked door" +
				"\nOn the northern side of the room..." + "There is a locked door" +
				"\nOn the eastern side of the room..." + "There is a locked door";
		assertEquals(result, room.describe());
	}
	
	@Test
	public void testAlterWays(){
		assertEquals("There is a locked door", room.getWayStateDescription(0));
		room.alterWay(0);
		assertEquals("You can quit the room by this way", room.getWayStateDescription(0));
	}

}
